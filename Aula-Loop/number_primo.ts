//Escreva um programa TypeScript que imprima todos os números primos de 1 a 53 usando a função while.

namespace number_primo {
  let numero, aux: number;
  let primo: boolean;

  //Fazer o numero chegar até 53
  numero = 0;
  while (numero <= 53) {
    aux = 2;
    primo = true;
    while (aux < numero) {
      if (numero % aux == 0)
    {
        primo = false;
        break;
      }
      aux++;
    }
    if (primo ==true && numero >1)
    {console.log(numero);
    }
    numero++;
  }
}
