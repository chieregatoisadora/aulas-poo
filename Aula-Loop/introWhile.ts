namespace introWhile
{
    let contador:number;
        contador = 0;  
    
    while(contador <= 10)
    {
        console.log(`O valor do contador é igual: ${contador}`);
        
        //contador= contador+1;(outra opção)
        contador++
    }

    //testa no final
    let contador1 :number; 
    contador1= 0;
    do {
        console.log(`O valor do contador é igual: ${contador}`);
        contador++
        
    }while(contador <= 10)
    
}