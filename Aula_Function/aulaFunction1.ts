namespace AulaFunction1 {
    function saudacao(nome?:string){
        if(nome){
            console.log(`Olá, ${nome}`);//n tem concatenação
        }
        else{
            console.log("Olá, estranho");//tem concatenação 
        }
    }
    saudacao("Isadora");

    function potencia (base:number, expoente:number = 2 ){
        console.log(Math.pow (base, expoente));
    }
    potencia(2);
    potencia (2,3);
}