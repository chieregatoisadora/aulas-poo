namespace AulaFunction {
    //let numero1 :number=10;
    //let numero2: number= 5;
    //let soma : number;
    //soma= numero1 + numero2;
    //console.log(soma);
    
    function soma (numero1:number , numero2: number)
    {
        let soma: number;
        soma = numero1 +numero2
        return soma
        //return numero1 + numero2 (jeito mais rapido)
    }
    console.log(soma (10, 5));
    
    function diferença (numero1:number , numero2: number)
    {
        let diferença: number;
        diferença= numero1 - numero2
        return diferença
        //return numero1 - numero2 (jeito mais rapido)
    }
    console.log(diferença (200, 15));

    function multiplicação (numero1:number , numero2: number)
    {
        let multiplicação: number;
        multiplicação = numero1 * numero2
        return multiplicação
        //return numero1 * numero2 (jeito mais rapido)
    }
    console.log(multiplicação (12, 12));

    function divisão (numero1:number , numero2: number)
    {
        let divisão : number;
        divisão  = numero1 / numero2
        return divisão 
        //return numero1 / numero2 (jeito mais rapido)
    }
    console.log(divisão  (144, 12));
}
