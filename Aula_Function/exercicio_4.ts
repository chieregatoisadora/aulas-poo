//Crie uma função que receba um número como parâmetro e retorne "par" se o número for par e "ímpar" se o número for ímpar.
namespace ex_4 {
    function parOuImpar(numero:number) 
    {
        if (numero / 2 == 0) {
            console.log(`${numero} é par`);
        }
        else {
            console.log(`${numero} é ímpar`);
            
        }
    }
    console.log(parOuImpar(77));
}