//Escreva um programa que pergunte ao usuário qual o seu nível de conhecimento em TypeScript e exiba uma mensagem de acordo com o nível escolhido

namespace exercicio_1 {
  let conhecimento: string;
  conhecimento = "iniciante";

  switch (conhecimento) {

  case "iniciante":
  console.log("O usuario está no inicio do conhecimento em TypeScript!");
  break;

  case "pre_intermediario":
  console.log("O usuario está no processo do conhecimento em TypeScript!");
  break;

  case "intermediario":
  console.log("O usuario está no meio do processo do conhecimento em TypeScript!");
  break;

  case "avançado":
  console.log("O usuario atingiu o máximo de conhecimento em TypeScript!!!");
  break;
  }
}
