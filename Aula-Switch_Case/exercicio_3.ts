//Escreva um programa que pergunte ao usuário qual o seu sabor de sorvete favorito e exiba uma mensagem de acordo com o sabor escolhido:

namespace exercicio_3 {
    let sorvete :string;
    sorvete = "chocolate";

    switch (sorvete) {

    case "chocolate":
    console.log ("O sorvete preferido do usuario é de chocolate!");
    break;

    case "morango":
    console.log("O sorvete preferido do usuario é de morango!");
    break;

    case "flocos":
    console.log("O sorvete preferido do usuario é de flocos!");
    break;

    case "napolitano":
    console.log ("O sorvete preferido do usuario é de napolitano");
    break;

    case "limão":
    console.log ("O sorvete preferido do usuario é de limão");
    break;

    case "maracuja":
    console.log ("O sorvete preferido do usuario é de maracuja");
    break;

    case "doce de leite":
    console.log ("O sorvete preferido do usuario é de doce de leite");
    break;

    case "passas":
    console.log ("O sorvete preferido do usuario é de passas");
    break;
    
    default:
    console.log ("O sorvete do usuario não está disponivel ):");
    
}
}