//Crie um array com 3 nomes de frutas. Em seguida, use um loop while para iterar sobre o array e exibir cada fruta em uma linha separada.

namespace frutas
{
    let fruit: string[]= ["Morango", "Maracujá", "Melancia"];
    let i :number = 0;

    while(i< fruit.length)
    {
        console.log(fruit[i]);
        i++;
    }
}