//crie um vetor chamado "alunos" contendo tres objetos,cada um representando um aluno com as seguintes propriedades "nome"(string), "idade"(number), e "notas"(array de num).Preencha o valor com informacçoes ficticias.
//percorra o vetor utilizando a função "forEach" e para cada aluno,calcule a media das notas e imprima  o resultado na tela,juntamente com o nome e a idade do aluno.

namespace ex_6{
    interface Alunos {
        nome: string;
        idade: number;
        notas: number[];
    }
    const alunos: Alunos[] = [
        {nome: "Isa", idade: 26, notas:[8, 7, 10]}, 
        {nome: "Mari", idade: 23, notas:[7, 10, 5]},
        {nome: "Ana", idade: 21, notas:[4, 9, 3]}
    ]
    alunos.forEach((aluno) => {
        let media = aluno.notas.reduce((total,nota)=> {return total+nota}) / aluno.notas.length

        if(media >= 7){
            console.log(`A media do aluno :${aluno.nome} é igual a :${media} e está aprovado`);
        }

        else {
            console.log(`A media do aluno :${aluno.nome} é igual a :${media} e está reprovado `);
        }    
    });
}
