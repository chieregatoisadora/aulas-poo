//Crie um array com 4 objetos, cada um representando um livro com as propriedades titulo e autor. Em seguida, use o método map() para criar um novo array contendo apenas os títulos dos livros.

namespace books {
  let book: any[] = [
    { tit: "Amor", aut: "Maria" },
    { tit: "Coragem", aut: "João" },
    { tit: "Bravura", aut: "João" },
    { tit: "Temor", aut: "João" },
    { tit: "Raiva", aut: "Vivi" },
  ];

  let titulos = book.map(function(tit){
    return tit.tit;
  });


  console.log (titulos);

  let joao = book.filter((book) => {
    return book.aut === "João"
  });

  console.log(joao);

  let titulo = joao.map(function(joao){
    return joao.tit
  });

  console.log(titulo);


}
